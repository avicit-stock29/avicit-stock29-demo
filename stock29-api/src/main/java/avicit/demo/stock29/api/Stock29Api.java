package avicit.demo.stock29.api;

import avicit.business.core.rest.client.RestClient;
import avicit.business.core.rest.client.RestClientUtils;
import avicit.business.core.rest.msg.QueryReqBean;
import avicit.business.core.rest.msg.QueryRespBean;
import avicit.business.core.rest.msg.ResponseMsg;
import avicit.demo.stock29.dto.Stock29DTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.util.List;

/**
* @金航数码科技有限责任公司
* @作者：gtest
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-29 16:17
* @类说明：stock29Api
* @修改记录：
*/
@Component
public class Stock29Api {

    /** 服务编码 */
    private static final String SERVICE_CODE = "stock29";
    private static final String BASE_PATH = "/api/demo/stock29/stock29Rest";
    
    @Autowired
    private RestClient restClient;

    /**
     * 按条件分页查询
     *
     * @param queryReqBean 查询条件
     * @return QueryRespBean <Stock29DTO>
     */
    public QueryRespBean<Stock29DTO> searchByPage(QueryReqBean<Stock29DTO> queryReqBean) {
        String url = BASE_PATH+"/searchByPage/v1";
        ResponseMsg<QueryRespBean<Stock29DTO>> responseMsg = restClient.doPost(SERVICE_CODE, url, queryReqBean, new ParameterizedTypeReference<ResponseMsg<QueryRespBean<Stock29DTO>>>() {
        });
        return RestClientUtils.getResponseBody(responseMsg);
    }
    
    /**
	 * 新增对象
	 *
	 * @param mpmUserInfoList 保存对象
	 * @return String
	 */
	public Void save(List<Stock29DTO> mpmUserInfoList) {
        String url = BASE_PATH+"/save/v1";
        ResponseMsg<Void> responseMsg = restClient.doPost(SERVICE_CODE, url, mpmUserInfoList, new ParameterizedTypeReference<ResponseMsg<Void>>() {
        });
        return RestClientUtils.getResponseBody(responseMsg);
    }

	/**
     * 按主键单条删除
     *
     * @param id 主键id
     * @return Integer
     */
    public Integer deleteById(String id) {
        String url = BASE_PATH+"/deleteById/v1";
        ResponseMsg<Integer> responseMsg = restClient.doPost(SERVICE_CODE, url, id, new ParameterizedTypeReference<ResponseMsg<Integer>>() {
        });
        return RestClientUtils.getResponseBody(responseMsg);
    }
	
	/**
     * 批量删除
     *
     * @param ids 逗号分隔的id串
     * @return Integer
     */
    public Integer deleteByIds(String ids) {
        String url = BASE_PATH+"/deleteByIds/v1";
        ResponseMsg<Integer> responseMsg = restClient.doPost(SERVICE_CODE, url, ids, new ParameterizedTypeReference<ResponseMsg<Integer>>() {
        });
        return RestClientUtils.getResponseBody(responseMsg);
    }

}
