package avicit.demo.stock29.dto;

import avicit.business.core.annotation.log.FieldRemark;
import avicit.business.core.annotation.log.Id;
import avicit.business.core.annotation.log.LogField;
import avicit.business.core.domain.BeanDTO;
import avicit.business.core.properties.PlatformConstant;
import avicit.business.core.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
* @金航数码科技有限责任公司
* @作者：gtest
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-29 16:17
* @类说明：stock29Dto
* @修改记录：
*/
@ApiModel(value = "Stock29DTO", description = "stock29")
public class Stock29DTO extends BeanDTO{
	private static final long serialVersionUID = 1L;

	/**
	* ID
	*/
	@Id
	@LogField
	@ApiModelProperty(value = "ID", name = "id")
	@FieldRemark(column="ID", field="id", name="ID")
	private java.lang.String id;

	/**
	* 商品id
	*/
	@LogField
	@ApiModelProperty(value = "商品id", name = "goodsId")
	@FieldRemark(column="GOODS_ID", field="goodsId", name="商品id")
	private java.lang.String goodsId;

	/**
	* 库存数量
	*/
	@LogField
	@ApiModelProperty(value = "库存数量", name = "num")
	@FieldRemark(column="NUM", field="num", name="库存数量")
	private java.lang.Integer num;

	/**
	* 组织ID
	*/
	@LogField
	@ApiModelProperty(value = "组织ID", name = "orgId")
	@FieldRemark(column="ORG_ID", field="orgId", name="组织ID")
	private java.lang.String orgId;

	/**
	* 部门ID
	*/
	@LogField
	@ApiModelProperty(value = "部门ID", name = "deptId")
	@FieldRemark(column="DEPT_ID", field="deptId", name="部门ID")
	private java.lang.String deptId;

	/**
	* 系统标识ID
	*/
	@LogField
	@ApiModelProperty(value = "系统标识ID", name = "sysId")
	@FieldRemark(column="SYS_ID", field="sysId", name="系统标识ID")
	private java.lang.String sysId;

	/**
	* 密级
	*/
		@LogField
		@ApiModelProperty(value = "密级", name = "secretLevel")
	@FieldRemark(column="SECRET_LEVEL", field="secretLevel", name="密级", dataType="lookup", lookupType="PLATFORM_FILE_SECRET_LEVEL")
	private java.lang.String secretLevel;
	private java.lang.String secretLevelName;


	/**
	* 多应用ID
	*/
	@LogField
	@ApiModelProperty(value = "多应用ID", name = "sysApplicationId")
	@FieldRemark(column="SYS_APPLICATION_ID", field="sysApplicationId", name="多应用ID")
	private java.lang.String sysApplicationId;

	/**
	* 组织标识
	*/
	@LogField
	@ApiModelProperty(value = "组织标识", name = "orgIdentity")
	@FieldRemark(column="ORG_IDENTITY", field="orgIdentity", name="组织标识")
	private java.lang.String orgIdentity;

	public java.lang.String getId(){
		return id;
	}

	public void setId(java.lang.String id){
		this.id = id;
	}
	public java.lang.String getGoodsId(){
		return goodsId;
	}

	public void setGoodsId(java.lang.String goodsId){
		this.goodsId = goodsId;
	}
	public java.lang.Integer getNum(){
		return num;
	}

	public void setNum(java.lang.Integer num){
		this.num = num;
	}
	public java.lang.String getOrgId(){
		return orgId;
	}

	public void setOrgId(java.lang.String orgId){
		this.orgId = orgId;
	}
	public java.lang.String getDeptId(){
		return deptId;
	}

	public void setDeptId(java.lang.String deptId){
		this.deptId = deptId;
	}
	public java.lang.String getSysId(){
		return sysId;
	}

	public void setSysId(java.lang.String sysId){
		this.sysId = sysId;
	}

	public java.lang.String getSecretLevel(){
		return secretLevel;
	}

	public void setSecretLevel(java.lang.String secretLevel){
		this.secretLevel = secretLevel;
	}

	public java.lang.String getSecretLevelName(){
		return secretLevelName;
	}

	public void setSecretLevelName(java.lang.String secretLevelName){
		this.secretLevelName = secretLevelName;
	}
	public java.lang.String getSysApplicationId(){
		return sysApplicationId;
	}

	public void setSysApplicationId(java.lang.String sysApplicationId){
		this.sysApplicationId = sysApplicationId;
	}
	public java.lang.String getOrgIdentity(){
		return orgIdentity;
	}

	public void setOrgIdentity(java.lang.String orgIdentity){
		this.orgIdentity = orgIdentity;
	}

	@Override
	public String getLogFormName() {
		if (StringUtils.isEmpty(super.logFormName)) {
			return "stock29";
		}else{
			return super.logFormName;
		}
	}

	@Override
	public String getLogTitle() {
		if (StringUtils.isEmpty(super.logTitle)) {
			return "stock29";
		}else{
			return super.logTitle;
		}
	}

	@Override
	public PlatformConstant.LogType getLogType() {
		if (super.logType == null) {
			return PlatformConstant.LogType.module_operate;
		} else {
			return super.logType;
		}
	}
}
