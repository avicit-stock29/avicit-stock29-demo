package avicit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
* 需要配置@MapperScan，自动扫描mybatis的mapper接口
*/
@SpringBootApplication
@MapperScan("avicit.**.dao")
public class Stock29Application {

    public static void main(String[] args) {
        SpringApplication.run(Stock29Application.class, args);
    }
}

