package avicit.demo.stock29.rest;

import avicit.business.core.contextThread.ContextCommonHolder;
import avicit.business.core.excel.export.ServerExcelExport;
import avicit.business.core.excel.export.headersource.VueExportDataGridHeaderSource;
import avicit.business.core.rest.msg.QueryReqBean;
import avicit.business.core.rest.msg.QueryRespBean;
import avicit.business.core.rest.msg.ResponseMsg;
import avicit.business.core.utils.JsonHelper;
import avicit.business.core.utils.StringUtils;
import avicit.business.module.system.SystemConstant;
import avicit.business.module.system.client.ConvertColumnClient;
import avicit.demo.stock29.dto.Stock29DTO;
import avicit.demo.stock29.service.Stock29Service;
import com.fasterxml.jackson.core.type.TypeReference;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static avicit.business.core.utils.BusinessUtil.buildExcelFile;
import static avicit.business.core.utils.BusinessUtil.getSortExpColumnName;
import static avicit.business.module.system.client.ConvertColumnClient.convertFormat;
import static avicit.business.module.system.client.ConvertColumnClient.createConvertSet;

/**
* @金航数码科技有限责任公司
* @作者：gtest
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-29 16:17
* @类说明：stock29Rest
* @修改记录：
*/
@RestController
@Api(tags = "stock29", description = "stock29")
@RequestMapping("/api/demo/stock29/Stock29Rest")
public class Stock29Rest {

    private static final Logger LOGGER = LoggerFactory.getLogger(Stock29Rest.class);

    @Autowired
    private Stock29Service stock29Service;

    @Autowired
    private ConvertColumnClient convertColumnClient;

    /**
     * 按条件分页查询
     *
     * @param queryReqBean 查询条件
     * @return ResponseMsg<QueryRespBean < Stock29DTO>>
     */
    @PostMapping("/searchByPage/v1")
    @ApiOperation(value = "按条件分页查询")
    public ResponseMsg<QueryRespBean<Stock29DTO>> searchByPage(@ApiParam(value = "查询条件", name = "queryReqBean") @RequestBody QueryReqBean<Stock29DTO> queryReqBean) {
        ResponseMsg<QueryRespBean<Stock29DTO>> responseMsg = new ResponseMsg<>();
        if (StringUtils.isNotEmpty(queryReqBean.getSidx()) && StringUtils.isNotEmpty(queryReqBean.getSord())) {
            String sordExp = getSortExpColumnName(queryReqBean.getSidx(), queryReqBean.getSord(), Stock29DTO.class);
            if (StringUtils.isNotEmpty(sordExp)) {
                queryReqBean.setSortExp(sordExp);
            }
        }
        if (StringUtils.isNotEmpty(queryReqBean.getKeyWord())) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Stock29DTO searchKeyWordParam = JsonHelper.getInstance().readValue(queryReqBean.getKeyWord(), dateFormat, new TypeReference<Stock29DTO>() {
            });
            queryReqBean.setSearchParams(searchKeyWordParam);
        }
        //文档密级
        String wordSecret = ContextCommonHolder.getWordSecret();
        QueryRespBean<Stock29DTO> result = stock29Service.searchStock29ByPage(queryReqBean, ContextCommonHolder.getOrgId(), wordSecret, queryReqBean.getSortExp());
        valueConvert(result.getResult());
        responseMsg.setResponseBody(result);
        return responseMsg;
    }

    /**
     * 按条件导出
     *
     * @param serverExp 查询条件
     * @return ResponseMsg<List < Stock29DTO>>
     */
    @PostMapping("/exportServerData/v1")
    @ApiOperation(value = "按条件导出")
    public void exportServerData(@ApiParam(value = "导出条件", name = "serverExcelExport") @RequestBody ServerExcelExport<Stock29DTO> serverExp, HttpServletResponse response) throws Exception {
        QueryReqBean<Stock29DTO> queryReqBean = serverExp.getQueryReqBean();
        //文档密级
        String wordSecret = ContextCommonHolder.getWordSecret();
        List<Stock29DTO> result = stock29Service.searchStock29ForExportExcel(queryReqBean, ContextCommonHolder.getOrgId(), wordSecret, queryReqBean.getSortExp());
        valueConvert(result);
        VueExportDataGridHeaderSource exportDataGridHeaderSource = new VueExportDataGridHeaderSource(serverExp.getDataGridheaders());
        exportDataGridHeaderSource.setUnexportColumn(serverExp.getUnContainFields());
        serverExp.setUserDefinedGridHeader(exportDataGridHeaderSource);
        Workbook workbook = serverExp.exportData(result);
        //生成excel文件并进行下载
        buildExcelFile(workbook, serverExp.getSheetName(), response);
    }

    /**
     * 保存对象
     *
     * @param stock29List 保存对象
     * @return ResponseMsg<Void>
     */
    @PostMapping("/save/v1")
    @ApiOperation(value = "保存对象")
    public ResponseMsg<Void> save(@ApiParam(value = "保存对象", name = "stock29List") @RequestBody List<Stock29DTO> stock29List) {
        ResponseMsg<Void> responseMsg = new ResponseMsg<>();
        stock29Service.insertOrUpdateStock29(stock29List, ContextCommonHolder.getOrgId());
        return responseMsg;
    }

    /**
     * 按主键单条删除
     *
     * @param id 主键id
     * @return ResponseMsg<Integer>
     */
    @PostMapping("/deleteById/v1")
    @ApiOperation(value = "按主键单条删除")
    public ResponseMsg<Integer> deleteById(@ApiParam(value = "主键id", name = "id") @RequestBody String id) {
        ResponseMsg<Integer> responseMsg = new ResponseMsg<>();
        responseMsg.setResponseBody(stock29Service.deleteStock29ById(id));
        return responseMsg;
    }

    /**
     * 批量删除
     *
     * @param ids 逗号分隔的id串
     * @return ResponseMsg<Integer>
     */
    @PostMapping("/deleteByIds/v1")
    @ApiOperation(value = "批量删除")
    public ResponseMsg<Integer> deleteByIds(@ApiParam(value = "逗号分隔的id串", name = "ids") @RequestBody String ids) {
        ResponseMsg<Integer> responseMsg = new ResponseMsg<>();
        responseMsg.setResponseBody(stock29Service.deleteStock29ByIds(ids.split(",")));
        return responseMsg;
    }

    /**
     * 通过平台API将字段值转换为名称，包括通用选择组件、通用代码
     *
     * @param Stock29DTOList
     */
    private void valueConvert(List<Stock29DTO> Stock29DTOList) {
        //循环组装请求数据
        Map<String, Set<String>> convertFormData = new HashMap<>();
        for (Stock29DTO stock29 : Stock29DTOList) {
            createConvertSet(convertFormData, "PLATFORM_FILE_SECRET_LEVEL", stock29.getSecretLevel());
        }
        if (convertFormData.size() > 0) {
            //获取请求结果
            Map<String, Map<String, String>> convertResultData = convertColumnClient.replace(convertFormData);
            //循环设置Alias或Name的值
            for (Stock29DTO stock29 : Stock29DTOList) {
                stock29.setSecretLevelName(convertFormat(convertResultData, "PLATFORM_FILE_SECRET_LEVEL", stock29.getSecretLevel()));
            }
        }
    }

}
