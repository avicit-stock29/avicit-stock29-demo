package avicit.demo.stock29.service;

import avicit.business.core.exception.BusinessException;
import avicit.business.core.log.SysLogUtil;
import avicit.business.core.mybatis.pagehelper.PageHelper;
import avicit.business.core.properties.PlatformConstant;
import avicit.business.core.rest.msg.QueryReqBean;
import avicit.business.core.rest.msg.QueryRespBean;
import avicit.business.core.utils.ComUtil;
import avicit.business.core.utils.PojoUtil;
import avicit.business.core.utils.StringUtils;
import avicit.demo.stock29.dao.Stock29DAO;
import avicit.demo.stock29.dto.Stock29DTO;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @金航数码科技有限责任公司
* @作者：gtest
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-29 16:17
* @类说明：stock29Service
* @修改记录：
*/
@Service
public class Stock29Service {

	private static final Logger LOGGER =  LoggerFactory.getLogger(Stock29Service.class);

	@Autowired
	private Stock29DAO stock29DAO;


    /**
	 * 按条件分页查询
	 *
	 * @param queryReqBean 查询条件
	 * @param orgIdentity 组织id
	 * @param wordSecret 文档密级
     * @param orderBy 排序
	 * @return QueryRespBean<Stock29DTO>
	 */
	@Transactional(readOnly = true)
	public QueryRespBean<Stock29DTO> searchStock29ByPage(QueryReqBean<Stock29DTO> queryReqBean, String orgIdentity, String wordSecret, String orderBy) {
		QueryRespBean<Stock29DTO> queryRespBean = new QueryRespBean<>();
		try{
			PageHelper.startPage(queryReqBean.getPageParameter());
            Stock29DTO searchParams = queryReqBean.getSearchParams();
			Page<Stock29DTO> dataList = stock29DAO.searchStock29ByPage(searchParams, orgIdentity, wordSecret, orderBy, queryReqBean.getKeyWord());
            queryRespBean.setResult(dataList);
			return queryRespBean;
		}catch(Exception e){
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("查询失败");
		}
    }

    /**
     * 按条件导出
     *
	 * @param queryReqBean 查询条件
	 * @param orgIdentity 组织id
	 * @param wordSecret 文档密级
	 * @param orderBy 排序
     * @return List<Stock29DTO>
     */
    @Transactional(readOnly = true)
    public List<Stock29DTO> searchStock29ForExportExcel(QueryReqBean<Stock29DTO> queryReqBean, String orgIdentity, String wordSecret, String orderBy){
        try{
            return stock29DAO.searchStock29ForExportExcel(queryReqBean.getSearchParams(),orgIdentity, wordSecret,orderBy,queryReqBean.getKeyWord());
        } catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("导出失败");
        }
    }

	/**
	 * 新增或修改对象
	 *
	 * @param stock29DTOList 对象集合
	 * @param orgIdentity 组织标识
	 * @return void
	 */
	 @Transactional
	public void insertOrUpdateStock29(List<Stock29DTO> stock29DTOList, String orgIdentity) {
		for(Stock29DTO stock29DTO : stock29DTOList){
			stock29DTO.setOrgIdentity(orgIdentity);
			if(StringUtils.isEmpty(stock29DTO.getId())){
				this.insertStock29(stock29DTO);
			}else{
				this.updateStock29(stock29DTO);
			}
		}
	}

	/**
	 * 新增对象
	 *
	 * @param stock29DTO 保存对象
	 * @return int
	 */
	@Transactional
	public int insertStock29(Stock29DTO stock29DTO) {
		try{
			stock29DTO.setId(ComUtil.getId());
			PojoUtil.setSysProperties(stock29DTO, PlatformConstant.OpType.insert);
			int ret = stock29DAO.insertStock29(stock29DTO);
			//记录日志
			SysLogUtil.log4Insert(stock29DTO);
			return ret;
		}catch(Exception e){
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("新增对象失败");
		}
	}
	
	/**
	 * 批量新增对象
	 *
	 * @param dtoList 保存对象集合
	 * @return int
	 */
	@Transactional
	public int insertStock29List(List<Stock29DTO> dtoList) {
		List<Stock29DTO> beanList = new ArrayList<>();
		for(Stock29DTO stock29DTO: dtoList){
			stock29DTO.setId(ComUtil.getId());
			PojoUtil.setSysProperties(stock29DTO, PlatformConstant.OpType.insert);
			SysLogUtil.log4Insert(stock29DTO);
			beanList.add(stock29DTO);
		}
		try{
		    return stock29DAO.insertStock29List(beanList);
		}catch(Exception e){
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("批量新增对象失败");
		}
	}
	
	/**
	 * 修改对象全部字段
	 *
	 * @param stock29DTO 修改对象
	 * @return int
	 */
	@Transactional
	public int updateStock29(Stock29DTO stock29DTO) {
		try {
			int ret = stock29DAO.updateStock29Sensitive(getUpdateDto(stock29DTO));
			if(ret == 0){
				throw new BusinessException("数据失效，请重新更新");
			}
			return ret;
        } catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("修改对象失败");
        }
		
	}
	
	/**
     * 内部方法，获取修改的dto对象
     *
     * @param stock29DTO
     * @return
     */
    private Stock29DTO getUpdateDto(Stock29DTO stock29DTO) {
    	Stock29DTO oldDTO = findById(stock29DTO.getId());
        if (oldDTO == null) {
            throw new BusinessException("数据不存在");
        }
        //记录日志
        SysLogUtil.log4Update(stock29DTO, oldDTO);
        PojoUtil.setSysProperties(stock29DTO, PlatformConstant.OpType.update);
        PojoUtil.copyProperties(oldDTO, stock29DTO, true);
        return oldDTO;
    }
	
	/**
	 * 批量更新对象
	 *
	 * @param dtoList 修改对象集合
	 * @return int
	 */
	@Transactional
	public int updateStock29List(List<Stock29DTO> dtoList) {
		List<Stock29DTO> beanList = new ArrayList<>();
        for (Stock29DTO stock29DTO : dtoList) {
        	Stock29DTO oldDTO = getUpdateDto(stock29DTO);
            beanList.add(oldDTO);
        }
		try{
            return stock29DAO.updateStock29List(beanList);
		}catch(Exception e){
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("批量更新对象失败");
		}
	}

	/**
	 * 按主键单条删除
	 *
	 * @param id 主键id
	 * @return int
	 */
	@Transactional
	public int deleteStock29ById(String id) {
		if(StringUtils.isEmpty(id)){
			throw new BusinessException("删除失败！传入的参数主键为null");
		}
		try{
			//记录日志
			Stock29DTO stock29DTO = findById(id);
			if(stock29DTO == null){
				throw new BusinessException("删除失败！对象不存在");
			}
			SysLogUtil.log4Delete(stock29DTO);
			return stock29DAO.deleteStock29ById(id);
		}catch(Exception e){
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("删除失败");
		}
	}
	
	/**
	 * 批量删除数据
	 *
	 * @param ids id的数组
	 * @return int
	 */
	@Transactional
	public int deleteStock29ByIds(String[] ids) {
		int result =0;
		for(String id : ids ){
			deleteStock29ById(id);
			result++;
		}
		return result;
	}

	/**
	 * 日志专用，内部方法，不再记录日志
	 *
	 * @param id 主键id
	 * @return Stock29DTO
	 */
	private Stock29DTO findById(String id) {
		return stock29DAO.findStock29ById(id);
	}

}
