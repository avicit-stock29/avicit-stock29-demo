package avicit.demo.stock29.dao;

import avicit.demo.stock29.dto.Stock29DTO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @金航数码科技有限责任公司
* @作者：gtest
* @邮箱：mm768528@163.com
* @创建时间： 2020-11-29 16:17
* @类说明：stock29Dao
* @修改记录：
*/
public interface Stock29DAO {

	/**
     * 分页查询  项目用户表
     *
     * @param stock29DTO 查询对象
     * @param orgIdentity 组织id
     * @param wordSecret 文档密级
     * @param orderBy 排序条件
     * @param keyWords 查询关键字
     * @return Page<Stock29DTO>
     */
	public Page<Stock29DTO> searchStock29ByPage(@Param("bean") Stock29DTO stock29DTO, @Param("orgIdentity") String orgIdentity, @Param("wordSecret") String wordSecret, @Param("orderBy") String orderBy, @Param("keyWords") String keyWords) ;

    /**
     * 查询项目用户表
     *
     * @param stock29DTO 查询对象
     * @return List<Stock29DTO>
     */
    public List<Stock29DTO> searchStock29(@Param("bean") Stock29DTO stock29DTO);

    /**
     * 按条件导出查询单表示例
     *
     * @param stock29DTO 查询对象
     * @param orgIdentity       组织id
     * @param wordSecret       文档密级
     * @param orderBy           排序条件
     * @param keyWords 查询关键字
     * @return Page<Stock29DTO>
     */
    public List<Stock29DTO> searchStock29ForExportExcel(@Param("bean") Stock29DTO stock29DTO, @Param("orgIdentity") String orgIdentity, @Param("wordSecret") String wordSecret, @Param("orderBy") String orderBy, @Param("keyWords") String keyWords);

    /**
     * 查询 项目用户表
     *
     * @param id 主键id
     * @return Stock29DTO
     */
    public Stock29DTO findStock29ById(String id);
    
        /**
     * 新增项目用户表
     *
     * @param stock29DTO 保存对象
     * @return int
     */
    public int insertStock29(Stock29DTO stock29DTO);
    
    /**
     * 批量新增 项目用户表
     *
     * @param dtoList 保存对象集合
     * @return int
     */
    public int insertStock29List(@Param("dtoList") List<Stock29DTO> dtoList);

    /**
     * 更新部分对象 项目用户表
     *
     * @param stock29DTO 更新对象
     * @return int
     */
    public int updateStock29Sensitive(Stock29DTO stock29DTO);
    
    /**
     * 更新全部对象 项目用户表
     *
     * @param stock29DTO 更新对象
     * @return int
     */
    public int updateStock29All(Stock29DTO stock29DTO);
    
    /**
     * 批量更新对象 项目用户表
     *
     * @param dtoList 批量更新对象集合
     * @return int
     */
    public int updateStock29List(@Param("dtoList") List<Stock29DTO> dtoList);
    
    /**
     * 按主键删除 项目用户表
     *
     * @param id 主键id
     * @return int
     */ 
    public int deleteStock29ById(String id);
    }
